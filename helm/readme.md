
# from here (./helm directory) use:

## To package the chart

     helm package k8s-oom-trackr

## To make the package and track generated name

     cd helm 
     export HELM_CHART_NAME=$(basename $(helm package k8s-oom-trackr | awk '{print $NF}'))
     echo $HELM_CHART_NAME 
     # To upload the package to gitlab registry
     curl --request POST \
          --form "chart=@$HELM_CHART_NAME" \
          --user cmorisse:$REPO_GIT_TOKEN \
          https://gitlab.com/api/v4/projects/53318069/packages/helm/api/stable/charts

     # To install the package
     helm repo add k8s-oom-tracker-charts https://gitlab.com/api/v4/projects/53318069/packages/helm/stable
     helm repo update k8s-oom-tracker-charts
     helm install one k8s-oom-tracker/inouk-oomkilled-tracker

