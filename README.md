# Inouk Kubernetes OOMKilled tracker

A Pod to log all pods that are killed with a OOMKilled reason.

### Differences with xing/kubernetes-oom-event-generator


The 2 solutions are very closed.

  * xing/kubernetes-oom-event-generator generates events about OOMKilled when a pod is restarted.
  * oom-trackr logs each time an event is related to a pod that have been OOMKiiled. So oom-trackr is more verbose (with duplicated line) but is also easier to track. You can easily display logs in kubernetes dashboard and or create alerts on you logging platform (eg. Graylog).


## Usage

This Pod has no GUI of any sort. It justs print Pods that have been OOMKilled like that:

```
Pod 'oomtrigger-2903cf0d-deploy-5d69576b9b-8rprj' (namespace: 'default') has been OOMKilled at 2023-12-23 11:05:30+00:00.
Pod 'oomtrigger-2903cf0d-deploy-5d69576b9b-8rprj' (namespace: 'default') has been OOMKilled at 2023-12-23 11:11:05+00:00.
...
```

Since it is stateless and watch events (eg. backoff) some lines will be duplicated.

## Installation

### Run locally 

You must map a valid kubeconfig to `/home/muppy/.kube/config`

```
#using docker
docker run -t -v $HOME/.kube/config:/home/muppy/.kube/config registry.gitlab.com/inouk/k8s-oom-tracker/oomtrackr:latest
```

### Run in your kubernetes cluster (recommended)

You should run `k8s-oom-tracker` in your cluster.

You can run it in any namespace. We use 'inouk-oomtrackr' in the examples below.

#### Install using helm package

```
helm repo add k8s-oom-tracker-charts https://gitlab.com/api/v4/projects/53318069/packages/helm/stable
helm repo upgrade k8s-oom-tracker-charts
# Install
helm install -n inouk-oomtrackr --createnamespace one k8s-oom-tracker-charts/inouk-oomkilled-tracker

# Upgrade
helm upgrade -n inouk-oomtrackr one k8s-oom-tracker-charts/inouk-oomkilled-tracker

```

#### Install from cloned repo

```
# Install 
# cd into repository root
# To check what will be installed
helm install -n inouk-oomtrackr --createnamespace one ./helm/k8s-oom-trackr --dry-run

# When you're ready
helm install -n inouk-oomtrackr --create-namespace one ./helm/k8s-oom-trackr

# Upgrade 
helm upgrade  -n inouk-oomtrackr one ./helm/k8s-oom-trackr
```

#### Uninstall

```
helm delete -n inouk-oomtrackr one
```


## Support

Open gitlab ticket and hope.

## Roadmap

Nothing more planned for now.

## Contributing

We don't expect contribution on this project. But contact us if you have any idea.

## Versions

To be completed.

## Authors

* Cyril MORISSE

## License
This project is licenced under MIT.

## References

 * https://github.com/xing/kubernetes-oom-event-generator
 * https://www.redhat.com/en/blog/writing-custom-controller-python
   * Corresponding repo: https://github.com/karmab/samplecontroller  


## How to trigger an OOM Error

Create a Deployment by Copy / Pasting next lines using your favorite tool (eg. k9s or Kubernetes Dashboard)

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: oomtrigger-python
  namespace: default
  labels:
    k8s-app: oomtrigger-python
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: oomtrigger-python
  template:
    metadata:
      name: oomtrigger-python
      labels:
        k8s-app: oomtrigger-python
    spec:
      containers:
        - name: oomtrigger
          image: python
          command:
            - python
          args:
            - -c
            - |
              import time
              print("OOMTrigger creating sentinel file.", flush=True)
              with open("/tmp/imalive.txt", "a") as f: f.write("hello")
              print("OOMTrigger begins to sleep.", flush=True)
              time.sleep(30)
              print("OOMTrigger starts eating memory.", flush=True)
              mem_chunks = []
              while True:
                x = bytearray(100 * 1024 * 1024)  # 100Mb
                mem_chunks.append(x)
                time.sleep(5)
                print("Allocated 100 Mb. Waiting 5s before retrying", flush=True)
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
            requests:
              cpu: 200m
              memory: 500Mi
          securityContext:
            privileged: false
```

## What happens when same script is run via bash

Create a pod with this manifest and check logs.
You will notice that the pod is not killed by Kubenetes but by bash

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: oomtrigger-python-in-bash
  namespace: default
  labels:
    k8s-app: oomtrigger-python-in-bash
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: oomtrigger-python-in-bash
  template:
    metadata:
      name: oomtrigger-python-in-bash
      labels:
        k8s-app: oomtrigger-python-in-bash
    spec: 
      containers:
        - name: oomtrigger-python-in-bash
          image: ubuntu
          command:
            - "/bin/bash"
          args:
            - "-c"
            - "apt update && apt install -y curl htop vim python3 && curl -fsSL https://gitlab.com/api/v4/projects/53318069/repository/files/oom-trigger.py/raw | python3"
          resources:
            requests:
              cpu: 100m
              memory: 100Mi
            limits:
              cpu: 500m
              memory: 500Mi
          env:
            - name: DEBIAN_FRONTEND
              value: noninteractive              

```

### Other ref material

```yaml
DEBIAN_FRONTEND=noninteractive

apt update && apt install -y htop curl vim python3

cat << 'EOF' > oom_trigger.py
import time
print("OOMTrigger creating sentinel file.", flush=True)
with open("/tmp/imalive.txt", "a") as f: f.write("hello")
print("OOMTrigger begins to sleep.", flush=True)
time.sleep(30)
print("OOMTrigger starts eating memory.", flush=True)
mem_chunks = []
while True:
  x = bytearray(100 * 1024 * 1024)  # 100Mb
  mem_chunks.append(x)
  time.sleep(5)
  print("Allocated 100 Mb. Waiting 5s before retrying", flush=True)
EOF

```
