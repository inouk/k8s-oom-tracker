#!/opt/muppy/k8s-oom-tracker/py3x/bin/python

"""
Author: Cyril MORISSE
(c) 2023 Cyril MORISSE
Licence: MIT
"""

import sys
import yaml
import json
import os
import pprint
import kubernetes
from kubernetes import client, config, watch


#dyn_client = kubernetes.dynamic.client.DynamicClient(k8s_client)


if __name__=='__main__':
    print("Starting k8s-oom-tracker", flush=True)
    if 'KUBERNETES_PORT' in os.environ:
        print("Loading 'incluster' config.", flush=True)
        config.load_incluster_config()
    else:
        print("Loading standard kubeconfig file.", flush=True)
        config.load_kube_config()

    LOG_GCED = os.environ.get("LOG_GCED", '').lower() in ('true', '1', 'yes', 'y',)

    k8s_client = kubernetes.client.ApiClient()
    coreV1 = kubernetes.client.CoreV1Api()

    while True:
        evt_stream = watch.Watch().stream(coreV1.list_event_for_all_namespaces)
        for event in evt_stream:
            #del event['raw_object']
            #print(event['object'])
            #print(event['raw_object'])

            if event['object'].involved_object.kind == 'Pod':
                _l0 = k8s_client.sanitize_for_serialization(event) 
                del _l0['object']['metadata']['managedFields']
                #print("involvedObject=%s" % _l0['object']['involvedObject'])
                _ns = _l0['object']['metadata'].get('namespace')
                _pod_uid = _l0['object']['involvedObject'].get('uid')
                _pod_name = _l0['object']['involvedObject'].get('name')
                try:
                    _r = coreV1.read_namespaced_pod_status(_pod_name, _ns, pretty=True)
                    for _cnt_status in _r.status.container_statuses or []:
                        if _cnt_status.last_state.terminated:
                            if _cnt_status.last_state.terminated.exit_code==137: 
                                #print("   %s" % k8s_client.sanitize_for_serialization(_cnt_status.last_state.terminated))
                                print(
                                    f"Pod '{_pod_name}' uid={_pod_uid} namespace='{_ns}' has been OOMKilled at "
                                    f"{_cnt_status.last_state.terminated.finished_at}.",
                                    flush=True
                                )

                except kubernetes.client.exceptions.ApiException as err:
                    if LOG_GCED:
                        print(
                            f"Pod '{_pod_name}' (namespace: '{_ns}') has been gced.", 
                            flush=True
                        )
