import time
print("OOMTrigger creating sentinel file.", flush=True)
with open("/tmp/imalive.txt", "a") as f: f.write("hello")
print("OOMTrigger begins to sleep.", flush=True)
time.sleep(30)
print("OOMTrigger starts eating memory.", flush=True)
mem_chunks = []
while True:
  x = bytearray(100 * 1024 * 1024)  # 100Mb
  mem_chunks.append(x)
  time.sleep(5)
  print("Allocated 100 Mb. Waiting 5s before retrying", flush=True)
  