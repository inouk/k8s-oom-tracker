#!/bin/bash

# Vérifie si un paramètre est passé au script
#if [ "$#" -eq 1 ]; then
#    # Affecte le paramètre à la variable d'environnement CURRENT_GIT_TAG
#    CURRENT_GIT_TAG=$1
#else
#    # Affecte le résultat de `git describe --tags` à CURRENT_GIT_TAG
#    CURRENT_GIT_TAG=$(git describe --tags)
#fi
#NORMALIZED_VERSION=$(py3x/bin/python .devcontainer/normalize_version.py $CURRENT_GIT_TAG)

#echo "Current Dir.       : $(pwd)"
#echo "Current Tag        : ${CURRENT_GIT_TAG}"
#read -p "Press [Enter] key to start build or CTRL-C to abort..."

docker build --build-arg REPO_GIT_TOKEN=$REPO_GIT_TOKEN --pull --no-cache --rm -f ".devcontainer/Dockerfile" -t oomtrackr:latest . 

# docker va produire une image en local
# qu'il faut tagger pour pouvoir la pousser dans la registry.
# Les lignes vont pousser l'image
docker tag oomtrackr:latest registry.gitlab.com/inouk/k8s-oom-tracker/oomtrackr:latest 
docker push                 registry.gitlab.com/inouk/k8s-oom-tracker/oomtrackr:latest 

#sleep 5s
#docker tag oomtrackr:latest registry.gitlab.com/inouk/k8s-oom-tracker/oomtracker:$CURRENT_GIT_TAG 
#docker push                 registry.gitlab.com/inouk/k8s-oom-tracker/oomtracker:$CURRENT_GIT_TAG
  
