# 
# Shell commands to build and run Ikb Odoo Docker 
#
# See: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
# First we must login info docker registry
# don't use sudo as build will fail
ù # Use a PAT

#
# Build 
# See: https://docs.gitlab.com/ee/user/packages/container_registry/ and heck naming convention
# This must be ran in the appserver / workspace folder
# Store the token in Dev Server ENV VARs => REPO_GIT_TOKEN
# 
docker build --build-arg REPO_GIT_TOKEN=$REPO_GIT_TOKEN --pull --no-cache --rm -f ".devcontainer/Dockerfile" -t ik_pg_health:latest . 
# ou
docker build --build-arg REPO_GIT_TOKEN=$REPO_GIT_TOKEN --pull            --rm -f ".devcontainer/Dockerfile" -t ik_pg_health:latest .   
#                                                                                                                             ^-context                      

# docker va produire une image en local
# qu'il faut tagger pour pouvoir la pousser dans la registry.
docker tag ik_pg_health:latest registry.gitlab.com/inouk/postgresql-health-check/ik_pg_health:latest 
docker push registry.gitlab.com/inouk/postgresql-health-check/ik_pg_health:latest 

docker tag ik_pg_health:latest docker push registry.gitlab.com/inouk/postgresql-health-check/ik_pg_health:1.0.0
docker push registry.gitlab.com/inouk/postgresql-health-check/ik_pg_health:1.0.0

#
# 
docker run -it registry.gitlab.com/cmorisse/appserver-mpy/mpy13c:latest /bin/bash

sudo docker run -p 8080:8080/tcp \
    --env PGHOST=$PGHOST \
    --env PGPORT=$PGPORT \
    --env PGUSER=$PGUSER \
    --env PGPASSWORD=$PGPASSWORD \
    -it --rm registry.gitlab.com/inouk/postgresql-health-check/ik_pg_health 

